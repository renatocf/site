module.exports = {
  future: {
    removeDeprecatedGapUtilities: true,
    purgeLayersByDefault: true,
  },
  purge: [],
  theme: {
    extend: {
      fontFamily: {
        display: ['Montserrat'],
        body: ['"Open Sans"'],
      },
    },
  },
  variants: {},
  plugins: [],
}
