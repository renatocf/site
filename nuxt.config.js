export default {
  // Target (https://go.nuxtjs.dev/config-target)
  target: 'static',

  // Global page headers (https://go.nuxtjs.dev/config-head)
  head: {
    htmlAttrs: {
      lang: 'en',
    },
    title: 'Alcnar | @renatocf page',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: 'Alcnar | @renatocf personal website & blog',
      },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'dns-prefetch', href: '//s.gravatar.com' },
      { rel: 'preconnect', href: '//s.gravatar.com', crossorigin: true },
    ],
  },

  // Global CSS (https://go.nuxtjs.dev/config-css)
  css: [],

  // Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
  plugins: [],

  // Auto import components (https://go.nuxtjs.dev/config-components)
  components: true,

  // Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module',
    // https://go.nuxtjs.dev/stylelint
    '@nuxtjs/stylelint-module',
    // https://go.nuxtjs.dev/tailwindcss
    '@nuxtjs/tailwindcss',
    // https://go.nuxtjs.dev/fontawesome
    '@nuxtjs/fontawesome',
    // https://go.nuxtjs.dev/google-fonts
    '@nuxtjs/google-fonts',
  ],

  // Modules (https://go.nuxtjs.dev/config-modules)
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    // https://go.nuxtjs.dev/content
    '@nuxt/content',
  ],

  // Axios module configuration (https://go.nuxtjs.dev/config-axios)
  axios: {},

  // Content module configuration (https://go.nuxtjs.dev/config-content)
  content: {},

  // FontAwesome module configuration (https://go.nuxtjs.dev/config-fontawesome)
  fontawesome: {
    icons: {
      brands: ['faTwitter', 'faGitlab', 'faGithub', 'faLinkedin'],
    },
  },

  // Google Fonts module configuration (https://go.nuxtjs.dev/config-google-fonts)
  googleFonts: {
    families: {
      Montserrat: true,
      'Open+Sans': true,
    },
    subsets: ['latin'],
    display: 'swap',
    download: true,
  },

  // Build Configuration (https://go.nuxtjs.dev/config-build)
  build: {},
}
